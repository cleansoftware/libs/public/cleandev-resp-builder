from flask import Response

from resp_builder import ResponseBuilder

if __name__ == '__main__':
    respose_builder = ResponseBuilder()
    response: Response = respose_builder.response(
        code='SUCCESS',
        data={'anykey': 'anyvalue'},
        status_code=200,
        is_merge=False
    )  # {"message": "Success", "data":{"anykey": "anyvalue"} http_code = 200
    print(response.json)